# Wver-webapp
The web client for wver

---

Star us on github! https://github.com/vaemoi-org/rev

## Getting Started

- Build the app:

```bash

npm run build

```

- Play with a local instance

```bash

not yet!

```

## Running tests
We've split our concerns about testing into two separate pieces (WE KNOW IT SEEMS WEIRD) and here's why:

>"I ain't got no type, code coverage is the only ting that I like"

When it comes to testing one wants to assure that what they are testing both delievers the desired result(s) and also did so in an expected way. Along with this many will say that focusing on the result is better (precision) and some will say that focusing on how the result was derived is better...some will say it's about the journey some will say it's about the destination...some will say the prequels ruined the series and some will say "Oke dae".

Long Story Short: Testing should be flexible and friendly for developers and by separating our BDD from our RDD we think we have a sweet spot.

... more on this soon.
