/**
 * Here is where users can view all of their revs
 *
 * Inbox => revs from other users
 * Outbox => revs sent to other users
 * Melee => revs sent to or from a group
 */
