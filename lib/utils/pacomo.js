import { withPackageName } from 'react-pacomo';

const {
  decorator: pacomoDecorator,
  transformer: pacomoTransformer
} = withPackageName(`wver`);

export {
  pacomoDecorator,
  pacomoTransformer
};
