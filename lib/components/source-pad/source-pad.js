/**
 * A allows users to:
 *   - View the content of each document
 *   - Switch between docments
 *   - Show diffs if available
 *   - See highlighted line references
 */
