import { Auth0Lock } from 'auth0-lock';
import { ManagementClient } from 'auth0';

/**
 * A set of helpers for handling authentication and identity of users with auth0.
 *
 * @param      {string}  clientId        Which client to auth for
 * @param      {string}  domain          The domain to auth on
 * @param      {object}  browserHistory  The App's browser
 * @param      {object}  apiClient       Service for accessing the revwver api
 *
 * @return     {object}  Authentication handling function and state
 */
const createAuthService = (clientId, domain, browserHistory, apiClient) => {
  const
    lock = new Auth0Lock(clientId, domain, {
      auth: {
        responseType: `token`
      },
      theme: {
        logo: `https://wver.vaemoi.co/assets/img/png/rev-icon.png`
      },
      languageDictionary: {
        title: `Log in to access wver`
      }
    }),

    setTokens = () => {},

    authenticate = () => {},

    getToken = () => {},

    login = () => {
      lock.show();
    },

    loggedIn = () => {},

    logout = () => {};

  lock.on(`authenticated`, authenticate);

  // Add other auth methods here

  return {
    authenticate,
    lock,
    getToken,
    login,
    loggedIn,
    logout
  };
};

export default createAuthService;
