import { createApolloFetch } from 'apollo-fetch';

/**
 * Creates a clients for our serverless backend api.
 *
 * @param      {String}  apiUri  The regular api url
 *
 * @return     {Object} An object containing the created api clients
 */
const apiService = (apiUri) => {
  // Create client
  const client = createApolloFetch({ uri: apiUri });

  // Add authentication middleware
  client.use(({request, options}, next) => {
    if (!options.headers) {
      options.headers = {};
    }

    options.headers[`authorization`] = ``;

    next();
  });


  // Add api methods here

  // Return methods but not the client
  return {};
};

export default apiService;
