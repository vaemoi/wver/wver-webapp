import {types, getEnv} from 'mobx-state-tree';

const BenchDocument = types.model(`BenchDocument`, {
  diff: types.string,
  id: types.identifier(types.number),
  name: types.string,
  syntax: types.string,
  url: types.string
});

const BenchFeedback = types.model(`BenchFeedback`, {
  author: types.string,
  author_img: types.string,
  line_refs: types.map(BenchFeedbackLineRef),
  id: types.identifier(types.number),
  message: types.string(),
  replies: types.map(BenchFeedbackReply)
});

const BenchFeedbackLineRef = types.model(`BenchFeedbackLineRef`, {
  id: types.identifier(types.number),
  document: types.reference(BenchDocument),
  lines: types.array(types.number)
});

const BenchFeedbackReply = types.model(`BenchFeedbackReply`, {
  author: types.string,
  author_img: types.string,
  id: types.identifier(types.number),
  message: types.string(),
  parent_id: types.reference(BenchFeedback),
});

const BenchStore = types.model(`BenchStore`, {
  creator: types.string,
  creator_img: types.string,
  current_line_refs: types.array(types.number),
  current_document: types.reference(BenchDocument),
  description: types.string,
  documents: types.map(BenchDocument),
  feedbacks: types.map(BenchFeedback),
  rev_id: types.identifier(types.number),
  show_diff: types.boolean,
  show_refs: types.boolean,
  title: types.string,
}.views((self) => ({
  get documents() {
    return self.documents.entries();
  },

  get feedbacks() {
    return self.documents.entries();
  }
})).actions((self) => {
  add_feedback(message, line_numbers, document_id) {
    const feedback = getEnv(self).api.add_feedback(message, line_numbers,
      document_id
    );

    self.feedbacks.set(feedback.id, feedback);
  },

  add_feedback_reply(feedback_id, message) {
    const
      reply = getEnv(self).api.add_feedback_reply(feedback_id, message),
      feedback = self.feedbacks.get(feedback_id);

    feedback.set(reply.id, reply);
  },

  delete_feedback(feedback_id) {
    const feedback = self.feedbacks.get(feedback_id);

    getEnv(self).api.delete_feedback(feedback_id);
    getEnv(self).api.delete_feedback(feedback.line_refs.keys);
    getEnv(self).api.delete_feedbackReply(feedback.replies.keys);

    self.feedbacks.delete(feedback_id);
  },

  switch_document(document_id) {
    self.current_line_refs = [];
    self.show_refs = false;
    self.current_document = document_id;
  },

  toggle_diff() {
    self.show_diff = !self.show_diff;
  },

  fetch_feedbacks() {
    new_feedbacks = getEnv(self).api.bench_feedbacks();

    self.feedbacks = new Map([self.documents, new_feedbacks]);
  },

  fetch_documents() {
    new_documents = getEnv(self).api.bench_documents();

    self.documents = new Map([self.documents, new_documents]);
  }
})):

export default BenchStore;
