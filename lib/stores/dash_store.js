import { types, getEnv } from 'mobx-state-tree';

const DashItem = types.model(`DashListItem`, {
  id: types.identifier(types.number),
  title: types.string,
  description: types.string,
  feedback_count: types.number,
  has_unreads: types.boolean,
  creator: types.string,
  creator_img: types.string,
  is_creator: types.boolean
});

const DashStore = types.model(`DashStore`, {
  inbox: types.map(DashItem),
  meleebox: types.map(DashItem),
  outbox: types.map(DashItem),
  current_tab: types.number,
}).views((self) => ({
  get items() {
    switch(self.current_tab) {
      case 1: return inbox.entries();
      case 2: return outbox.entries();
      case 3: return meleebox.entries();
    }
  }
})).actions((self) => ({
  delete_item(key) {
    switch(self.current_tab) {
      case 1: inbox.delete(key); break;
      case 2: meleebox.delete(key); break;
      case 3: outbox.delete(key); break;
    }

    getEnv(self).api.delete_rev(key);
  }

  fetch_items() {
    let new_items = null;

    switch(self.current_tab) {
      case 1:
        new_items = getEnv(self).api.fetch_inbox_revs();
        self.inbox = new Map([self.inbox, new_items]);
        break;
      case 2:
        new_items = getEnv(self).api.fetch_melee_revs();
        self.meleebox = new Map([self.meleebox, new_items]);
        break;
      case 3:
        new_items = getEnv(self).api.fetch_outbox_revs();
        self.outbox = new Map([self.outbox, new_items]);
        break;
    }
  },

  switch_tab(new_tab) {
    if self.current_tab !== new_tab {
      self.current_tab = new_tab;
    }
  }
}));

export default DashStore;
