// External modules
import createHistory from 'history/createBrowserHistory';
import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from 'react-junctions';

// Remove loading animation and render Component into page
const spinny = document.querySelector(`#wver-root`);
spinny.firstChild.remove();


const root = document.querySelector(`#wver-root`);

ReactDOM.render(<div><span>{`Hello`}</span></div>, root);

// If we want to put the spinny back in case of errors or loading times
// before we delete it let's save it spinnyCopy = spinny.cloneNode(true);
