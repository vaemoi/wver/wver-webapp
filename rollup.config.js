import babel from 'rollup-plugin-babel';
import cjs from 'rollup-plugin-commonjs';
import nodeGlobals from 'rollup-plugin-node-globals';
import nodeResolve from 'rollup-plugin-node-resolve';
import uglify from 'rollup-plugin-uglify';

export default {
  external: [
    `apollo-fetch`,
    `auth0-lock`,
    `history`,
    `junctions`,
    `prop-types`,
    `react`,
    `react-codemirror`,
    `react-dom`,
    `react-junctions`,
    `react-pacomo`
  ],

  input: `${__dirname}/lib/wver.js`,

  output: {
    file: `${__dirname}/public/js/bundle.js`,
    format: `iife`,
    sourcemap: true,
    globals: {
      'apollo-fetch': `createApolloFetch`,
      'auth0': 'AuthenticationClient',
      'auth0': 'ManagementClient',
      'auth0-lock': `Auth0Lock`,
      'history': 'createHistory',
      'prop-types': 'PropTypes',
      'react': `React`,
      'react-dom': `ReactDom`,
      'react-junctions': `JunctionNavigation`,
      'react-junctions': `createJunctionTemplate`,
      'react-junctions': `createPageTemplate`,
      'react-junctions': `JunctionActiveChild`,
      'react-junctions': `Link`,
      'react-junctions': `Router`,
      'react-pacomo': `withPackageName`
    }
  },

  plugins: [
    babel({
      exclude: [`node_modules/**`, `spec/**`, `stories/**`],
    }),
    cjs(),
    nodeGlobals(),
    nodeResolve({
      browser: true,
      jsnext: true,
      main: true
    }),
    uglify()
  ]
};
