require(`browser-sync`)({
  files: [`${__dirname}/public/css/*.css`],
  open: false,
  plugins: [{
    module: `bs-html-injector`,
    options: {
      files: [`${__dirname}/public/*.html`]
    }
  }],
  server: `public`
});

console.log(`listening on w/ browser-sync`);
